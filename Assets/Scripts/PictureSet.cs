﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PictureSet : MonoBehaviour
{
    public GameObject Images;
    
    public Sprite[] Pets;
    public Sprite[] WildAnimals;
    public Sprite[] poultry;
    public Sprite[] WildBirds;
    public Sprite[] Fruits;
    public Sprite[] Vegetables;
    public Sprite[] INSECTS;
    public Sprite[] PlantsTree;
    public Sprite[] PlantsFlowers;
    
    public Image picture;
    
    // Start is called before the first frame update
    public void SetPicture(int pic, string category)
    {
        switch (category)
        {
            case "Pets":
                picture.sprite = Pets[pic];
                break;
            case "Wild animals":
                picture.sprite = WildAnimals[pic];
                break;
            case "poultry":
                picture.sprite = poultry[pic];
                break;
            case "WildBirds":
                picture.sprite = WildBirds[pic];
                break;
            case "Fruits":
                picture.sprite = Fruits[pic];
                break;
            case "Vegetables":
                picture.sprite = Vegetables[pic];
                break;
            case "INSECTS":
                picture.sprite = INSECTS[pic];
                break;
            case "PlantsTree":
                picture.sprite = PlantsTree[pic];
                break;
            case "PlantsFlowers":
                picture.sprite = PlantsFlowers[pic];
                break; 
                
                
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
