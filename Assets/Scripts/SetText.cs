﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SetText : MonoBehaviour
{
    
    public Text[] CategoryTexts;
    public TextAsset data;
    public PictureSet pictureSet;
    private List<string> Category = new List<string>();
    private string CategorySelected = "";
    private string LanguageSelected = "";
    public Text showItems;
    private int wordsInCategory = 0;
    public AudioSource audio;
    public int itemInCategory { get; set; }

    void Start()
    {
        SetDefaultLang();
        audio = GetComponent<AudioSource>();
        SetMainMenuButtons();

        //StartCoroutine(speech());
        Debug.Log(LanguageController.languagesPrefix);
        //Debug.Log(ReadItem(5, 9));
    }

    string ReadItem(int row, int item)
    {
        string result="";
        string[] linesFromfile = data.text.Split("\n"[0]);
        List<string> listB = new List<string>();
        for (int i = 0; i < linesFromfile.Length; i++)
        {
            listB.Add(linesFromfile[i]);
        }
        var rowSplit = listB[row].Split(',');
        result = rowSplit[item];
        return result.ToUpper();
    }

    IEnumerator speech(string text, string lang)
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            string url =
                "https://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q="+text.ToLower()+"&tl="+lang;
            Debug.Log(url);
            WWW www = new WWW(url);
            yield return www;
            audio.clip = www.GetAudioClip(false, true, AudioType.MPEG);
            audio.Play(); 
        }
        else
        {
            Debug.Log("No INTERNET");
        }
    }

    public void SetCategory(string category)
    {
        CategorySelected = category;
    }

    public void StartItemInCategory(int item)
    {
        itemInCategory = item;
    }

    public void NextItem()
    {
        itemInCategory++;
        Debug.Log(itemInCategory);
        SetCategoryItem();
    }
    public void PrewItem()
    {
        itemInCategory--;
        Debug.Log(itemInCategory);
        SetCategoryItem();
    }
    public void SetCategoryItem()
    {
        if (CategorySelected != string.Empty)
        {
            if (LanguageController.languagesPrefix =="ru")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;

                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    

                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;

                    var item= ReadItem(itemInCategory, 7);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="en")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 8);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="ro")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 9);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="fr")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 10);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="de")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 11);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="es")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 12);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="it")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 13);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="fi")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 14);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="ja")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 15);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="zh")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 16);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }
            if (LanguageController.languagesPrefix =="ko")
            {
                if (CategorySelected == "Pets")
                {
                    if (itemInCategory > 9)
                        itemInCategory = 2;
                    if (itemInCategory < 2)
                        itemInCategory = 9;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Wild animals")
                {
                    if (itemInCategory > 22)
                        itemInCategory = 11;
                    if (itemInCategory <11)
                        itemInCategory = 22;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "poultry")
                {
                    if (itemInCategory > 29)
                        itemInCategory = 24;
                    if (itemInCategory < 24)
                        itemInCategory = 29;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "WILD BIRDS")
                {
                    if (itemInCategory > 40)
                        itemInCategory = 31;
                    if (itemInCategory < 31)
                        itemInCategory = 40;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Fruits")
                {
                    if (itemInCategory > 52)
                        itemInCategory = 42;
                    if (itemInCategory < 42)
                        itemInCategory = 52;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Vegetables")
                {
                    if (itemInCategory > 68)
                        itemInCategory = 54;
                    if (itemInCategory < 54)
                        itemInCategory = 68;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "INSECTS")
                {
                    if (itemInCategory > 85)
                        itemInCategory = 70;
                    if (itemInCategory < 70)
                        itemInCategory = 85;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "PLANT TREES")
                {
                    if (itemInCategory > 103)
                        itemInCategory = 87;
                    if (itemInCategory < 87)
                        itemInCategory = 103;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }

                if (CategorySelected == "Plants Flowers")
                {
                    if (itemInCategory > 118)
                        itemInCategory = 105;
                    if (itemInCategory < 105)
                        itemInCategory = 118;
                    var item= ReadItem(itemInCategory, 17);
                    showItems.text = item;
                    StartCoroutine(speech(item,LanguageController.languagesPrefix));
                }
            }

            SetPictures();
        }
    }

    private void SetPictures()
    {
        switch (CategorySelected)
        {
            case "Pets":
                pictureSet.SetPicture(itemInCategory-2,"Pets");
                break;
            case "Wild animals":
                pictureSet.SetPicture(itemInCategory-11,"Wild animals");
                break;
            case "poultry":
                pictureSet.SetPicture(itemInCategory-24,"poultry");
                break;
            case "WILD BIRDS":
                pictureSet.SetPicture(itemInCategory-31,"WildBirds");
                break;
            case "Fruits":
                pictureSet.SetPicture(itemInCategory-42,"Fruits");
                break;
            case "Vegetables":
                pictureSet.SetPicture(itemInCategory-54,"Vegetables");
                break;
            case "INSECTS":
                pictureSet.SetPicture(itemInCategory-70,"INSECTS");
                break;
            case "PLANT TREES":
                pictureSet.SetPicture(itemInCategory-87,"PlantsTree");
                break;
            case "Plants Flowers":
                pictureSet.SetPicture(itemInCategory-105,"PlantsFlowers");
                break;
        }
    }
    
    private void SetDefaultLang()
    {
        switch (SystemLanguage.Russian.ToString())
        {
            case "Russian":
                LanguageController.languagesPrefix = "ru";
                break;
            case "English":
                LanguageController.languagesPrefix = "en";
                break;
            case "Romanian":
                LanguageController.languagesPrefix = "ro";
                break;
            case "French":
                LanguageController.languagesPrefix = "fr";
                break;
            case "German":
                LanguageController.languagesPrefix = "de";
                break;
            case "Spanish":
                LanguageController.languagesPrefix = "es";
                break;
            case "Italian":
                LanguageController.languagesPrefix = "it";
                break;
            case "Finnish":
                LanguageController.languagesPrefix = "fi";
                break;
            case "Japanese":
                LanguageController.languagesPrefix = "ja";
                break;
            case "Chinese":
                LanguageController.languagesPrefix = "zh";
                break;
            case "Korean":
                LanguageController.languagesPrefix = "ko";
                break;
            default:
                LanguageController.languagesPrefix = "en";
                break;
        } 
    }

    public void SetLanguage(string language)
    {
        LanguageSelected = language;
        LanguageController.languagesPrefix = language;
        if (language == "ru")
            LanguageController.Instance.CurrentLanguage = Language.Russian;
        if (language == "en")
            LanguageController.Instance.CurrentLanguage = Language.English;
        if(language =="ro")
            LanguageController.Instance.CurrentLanguage = Language.Romanian;
        if(language =="fr")
            LanguageController.Instance.CurrentLanguage = Language.French;
        if(language =="de")
            LanguageController.Instance.CurrentLanguage = Language.German;
        if(language =="es")
            LanguageController.Instance.CurrentLanguage = Language.Spanish;
        if(language =="it")
            LanguageController.Instance.CurrentLanguage = Language.Italian;
        if(language =="fi")
            LanguageController.Instance.CurrentLanguage = Language.Finnish;
        if(language =="ja")
            LanguageController.Instance.CurrentLanguage = Language.Japan;
        if(language =="zh")
            LanguageController.Instance.CurrentLanguage = Language.Chinese;
        if(language =="ko")
            LanguageController.Instance.CurrentLanguage = Language.Korean;

        SetMainMenuButtons();            
        Debug.Log(LanguageController.languagesPrefix);

    }

    public void SetMainMenuButtons()
    {
        if (LanguageController.languagesPrefix == "ru")
        {
            CategoryTexts[0].text = ReadItem(1, 7);
            CategoryTexts[1].text = ReadItem(10, 7);
            CategoryTexts[2].text = ReadItem(23, 7);
            CategoryTexts[3].text = ReadItem(30, 7);
            CategoryTexts[4].text = ReadItem(41, 7);
            CategoryTexts[5].text = ReadItem(53, 7);
            CategoryTexts[6].text = ReadItem(69, 7);
            CategoryTexts[7].text = ReadItem(86, 7);
            CategoryTexts[8].text = ReadItem(104, 7);
        }
        if (LanguageController.languagesPrefix == "en")
        {
            CategoryTexts[0].text = ReadItem(1, 8);
            CategoryTexts[1].text = ReadItem(10, 8);
            CategoryTexts[2].text = ReadItem(23, 8);
            CategoryTexts[3].text = ReadItem(30, 8);
            CategoryTexts[4].text = ReadItem(41, 8);
            CategoryTexts[5].text = ReadItem(53, 8);
            CategoryTexts[6].text = ReadItem(69, 8);
            CategoryTexts[7].text = ReadItem(86, 8);
            CategoryTexts[8].text = ReadItem(104, 8);
        }
        if (LanguageController.languagesPrefix == "ro")
        {
            CategoryTexts[0].text = ReadItem(1, 9);
            CategoryTexts[1].text = ReadItem(10, 9);
            CategoryTexts[2].text = ReadItem(23, 9);
            CategoryTexts[3].text = ReadItem(30, 9);
            CategoryTexts[4].text = ReadItem(41, 9);
            CategoryTexts[5].text = ReadItem(53, 9);
            CategoryTexts[6].text = ReadItem(69, 9);
            CategoryTexts[7].text = ReadItem(86, 9);
            CategoryTexts[8].text = ReadItem(104, 9);
        }
        if (LanguageController.languagesPrefix == "fr")
        {
            CategoryTexts[0].text = ReadItem(1, 10);
            CategoryTexts[1].text = ReadItem(10, 10);
            CategoryTexts[2].text = ReadItem(23, 10);
            CategoryTexts[3].text = ReadItem(30, 10);
            CategoryTexts[4].text = ReadItem(41, 10);
            CategoryTexts[5].text = ReadItem(53, 10);
            CategoryTexts[6].text = ReadItem(69, 10);
            CategoryTexts[7].text = ReadItem(86, 10);
            CategoryTexts[8].text = ReadItem(104, 10);
        }
        if (LanguageController.languagesPrefix == "de")
        {
            CategoryTexts[0].text = ReadItem(1, 11);
            CategoryTexts[1].text = ReadItem(10, 11);
            CategoryTexts[2].text = ReadItem(23, 11);
            CategoryTexts[3].text = ReadItem(30, 11);
            CategoryTexts[4].text = ReadItem(41, 11);
            CategoryTexts[5].text = ReadItem(53, 11);
            CategoryTexts[6].text = ReadItem(69, 11);
            CategoryTexts[7].text = ReadItem(86, 11);
            CategoryTexts[8].text = ReadItem(104, 11);
        }
        if (LanguageController.languagesPrefix == "es")
        {
            CategoryTexts[0].text = ReadItem(1, 12);
            CategoryTexts[1].text = ReadItem(10, 12);
            CategoryTexts[2].text = ReadItem(23, 12);
            CategoryTexts[3].text = ReadItem(30, 12);
            CategoryTexts[4].text = ReadItem(41, 12);
            CategoryTexts[5].text = ReadItem(53, 12);
            CategoryTexts[6].text = ReadItem(69, 12);
            CategoryTexts[7].text = ReadItem(86, 12);
            CategoryTexts[8].text = ReadItem(104, 12);
        } 
        if (LanguageController.languagesPrefix == "it")
        {
            CategoryTexts[0].text = ReadItem(1, 13);
            CategoryTexts[1].text = ReadItem(10, 13);
            CategoryTexts[2].text = ReadItem(23, 13);
            CategoryTexts[3].text = ReadItem(30, 13);
            CategoryTexts[4].text = ReadItem(41, 13);
            CategoryTexts[5].text = ReadItem(53, 13);
            CategoryTexts[6].text = ReadItem(69, 13);
            CategoryTexts[7].text = ReadItem(86, 13);
            CategoryTexts[8].text = ReadItem(104, 13);
        }
        if (LanguageController.languagesPrefix == "fi")
        {
            CategoryTexts[0].text = ReadItem(1, 14);
            CategoryTexts[1].text = ReadItem(10, 14);
            CategoryTexts[2].text = ReadItem(23, 14);
            CategoryTexts[3].text = ReadItem(30, 14);
            CategoryTexts[4].text = ReadItem(41, 14);
            CategoryTexts[5].text = ReadItem(53, 14);
            CategoryTexts[6].text = ReadItem(69, 14);
            CategoryTexts[7].text = ReadItem(86, 14);
            CategoryTexts[8].text = ReadItem(104, 14);
        }
        if (LanguageController.languagesPrefix == "ja")
        {
            CategoryTexts[0].text = ReadItem(1, 15);
            CategoryTexts[1].text = ReadItem(10, 15);
            CategoryTexts[2].text = ReadItem(23, 15);
            CategoryTexts[3].text = ReadItem(30, 15);
            CategoryTexts[4].text = ReadItem(41, 15);
            CategoryTexts[5].text = ReadItem(53, 15);
            CategoryTexts[6].text = ReadItem(69, 15);
            CategoryTexts[7].text = ReadItem(86, 15);
            CategoryTexts[8].text = ReadItem(104, 15);
        }
        if (LanguageController.languagesPrefix == "zh")
        {
            CategoryTexts[0].text = ReadItem(1, 16);
            CategoryTexts[1].text = ReadItem(10, 16);
            CategoryTexts[2].text = ReadItem(23, 16);
            CategoryTexts[3].text = ReadItem(30, 16);
            CategoryTexts[4].text = ReadItem(41, 16);
            CategoryTexts[5].text = ReadItem(53, 16);
            CategoryTexts[6].text = ReadItem(69, 16);
            CategoryTexts[7].text = ReadItem(86, 16);
            CategoryTexts[8].text = ReadItem(104, 16);
        }
        if (LanguageController.languagesPrefix == "ko")
        {
            CategoryTexts[0].text = ReadItem(1, 17);
            CategoryTexts[1].text = ReadItem(10, 17);
            CategoryTexts[2].text = ReadItem(23, 17);
            CategoryTexts[3].text = ReadItem(30, 17);
            CategoryTexts[4].text = ReadItem(41, 17);
            CategoryTexts[5].text = ReadItem(53, 17);
            CategoryTexts[6].text = ReadItem(69, 17);
            CategoryTexts[7].text = ReadItem(86, 17);
            CategoryTexts[8].text = ReadItem(104, 17);
        }
    }

    public void NextWordInCategory()
    {
        SetCategoryItem();
    }
}
